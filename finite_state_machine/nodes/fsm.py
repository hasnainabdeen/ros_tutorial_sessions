#!/usr/bin/env python

# Software License Agreement (BSD License)

# Copyright (c) 2021, Muhammad Husnain Ul Abdeen
# All rights reserved.

## This rospy service implements a finite state machine triggers state transitions and
## asynchronously publishes the current internal state on a topic

import rospy
import time
from finite_state_machine.msg import State
from finite_state_machine.srv import TriggerTransition, TriggerTransitionResponse

class FSM(object):
    def __init__(self):
        # current internal state object
        self.current_state = State()
        self.current_state.state = 0
        #self.current_state.state_alias = "STATE_0"
        self.current_state.state_name = State.STATE_0

        self.pub = rospy.Publisher("~state", State, queue_size=10)
        self.serv = rospy.Service("~trigger", TriggerTransition, self.trigger_transition)
        self.duration = rospy.get_param('~duration', 0.5)
        self.timer = rospy.Timer(rospy.Duration(self.duration), self.publish_state_async)
        rospy.spin()

    def trigger_transition(self, req):
        try:
            start = time.time()
            transition_name = getattr(req, "TRANSITION_%i"%req.transition)
        
            # states that have valid paths to the target state
            source_states = getattr(State, "SOURCE_TO_STATE_%i"%req.transition)
        
            # check whether the current state has a direct path to the target state
            #if str(self.current_state.state) in source_states.split(','):
            if self.current_state.state in list(map(int, source_states.split(','))):
                state_alias = "STATE_%i"%req.transition  
                self.current_state.state_name = getattr(State, state_alias)
                self.current_state.state = req.transition
                success = True
            else:
                success = False
            end = time.time()
            rospy.loginfo("execution time: %f, transition:%s -> success:%s, state:%s"%(end-start, transition_name, success, self.current_state.state_name)) 
            return TriggerTransitionResponse(success, self.current_state.state)
        except Exception as e:
            rospy.loginfo(e)
            import sys
            sys.exit(1)

    def publish_state_async(self, event=None):
        self.pub.publish(self.current_state)

if __name__ == "__main__":
    rospy.init_node("fsm_server")
    fsm = FSM()


#!/usr/bin/env python
import rospy
from std_msgs.msg import Float32

class TriangleGenerator:

    def __init__(self):
        self.pub = rospy.Publisher("~raw", Float32, queue_size=10)
        self.params()
        self.wave_properties()
        self.rate = rospy.Rate(self.frequency)

    def params(self):
        self.amplitude = rospy.get_param('~amplitude', 2)
        self.offset = rospy.get_param('~offset', 1) 
        self.frequency = rospy.get_param('~frequency', 3)
        self.signal_length = rospy.get_param('~signal_length', 9)

    def wave_properties(self):
        self.upper_peak = self.amplitude + self.offset
        self.lower_peak = self.offset - self.amplitude
        self.amp_step = 4*self.amplitude/(self.signal_length-1)

    def run(self):
        signal_value = float(self.offset)

        while not rospy.is_shutdown():
            self.rate.sleep()
            self.pub.publish(signal_value)
            if signal_value in (self.upper_peak, self.lower_peak):
                self.amp_step *= -1
            signal_value += self.amp_step

if __name__ == '__main__':

    try:
        rospy.init_node('triangle_generator', anonymous=False)

        tri_wave_gen = TriangleGenerator()
        tri_wave_gen.run()
    except rospy.ROSInterruptException:
        pass



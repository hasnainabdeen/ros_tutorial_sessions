#!/usr/bin/env python
import rospy
import statistics
from std_msgs.msg import Float32

class Averager:

    def __init__(self):
        self.sample_list = []
        self.pub = rospy.Publisher("~avg", Float32, queue_size=10)
        self.sub = rospy.Subscriber("~raw", Float32, self.callback) 
        self.buffer_size = rospy.get_param('~buffer_size', 10)

    def callback(self, sample):

        self.sample_list.append(sample.data)
    
        if len(self.sample_list) == self.buffer_size:

            avg = statistics.mean(self.sample_list)
            self.sample_list = []
            self.pub.publish(avg)

if __name__ == '__main__':

    rospy.init_node('averager', anonymous=False)

    Averager()

    rospy.spin()


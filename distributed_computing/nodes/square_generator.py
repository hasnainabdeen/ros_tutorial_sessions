#!/usr/bin/env python
import rospy
import statistics
from std_msgs.msg import Float32

class SquareGenerator:

    def __init__(self):
        self.value_ind = 0
        self.next_sig_values = []
        self.pub = rospy.Publisher("~raw", Float32, queue_size=10)
        self.params()
        self.wave()
        self.rate = rospy.Rate(self.frequency)
        
    def params(self):
        self.amplitude = rospy.get_param('~amplitude', 2)
        self.offset = rospy.get_param('~offset', 1) 
        self.frequency = rospy.get_param('~frequency', 3)
        self.signal_length = rospy.get_param('~signal_length', 9)
 
    def wave(self):
        upper_peak = self.amplitude + self.offset
        lower_peak = self.offset - self.amplitude
        duty_cyc = statistics.median(range(1, self.signal_length+1, 1))
        self.next_sig_values = [upper_peak if sig_ind < duty_cyc else self.offset if sig_ind == duty_cyc else lower_peak for sig_ind in range(1, self.signal_length+1, 1)]
        
    def run(self):
        while not rospy.is_shutdown():
            self.rate.sleep()
            #print(next_sig_values[value_ind])
            self.pub.publish(self.next_sig_values[self.value_ind])
            self.value_ind += 1
            if self.value_ind == self.signal_length:
                self.value_ind = 0
         

if __name__ == '__main__':

    try:
        rospy.init_node('square_generator', anonymous=False)

        sq_wave_gen = SquareGenerator()
        sq_wave_gen.run()
    except rospy.ROSInterruptException:
        pass

